#include <Vencoder.h>
#include <iostream>

vluint64_t main_time = 0;

double sc_time_stamp() { return main_time; }

int main(int argc, char **argv) {
  Verilated::commandArgs(argc, argv);
  Vencoder dut;
  bool pass = true;

  int forward_ticks = 8;
  int backward_ticks = 13;

  dut.eval();

  for (int i = 0; i < forward_ticks; ++i) {
    dut.signal = 1;
    dut.eval();
    dut.signal = 0;
    dut.eval();
  }

  if (dut.ticks != forward_ticks) {
    std::cerr << "Failed to count forwards. Ticks: Expected: "
              << (int)forward_ticks << " Got: " << dut.ticks << std::endl;
    pass = false;
  }

  dut.direction = true;

  for (int i = 0; i < backward_ticks; ++i) {
    dut.signal = 1;
    dut.eval();
    dut.signal = 0;
    dut.eval();
  }

  if (dut.ticks != forward_ticks - backward_ticks) {
    std::cerr << "Failed to count backwards. Ticks: Expected: "
              << forward_ticks - backward_ticks << " Got: " << (int)dut.ticks
              << std::endl;
    pass = false;
  }

  dut.final();

  if (pass)
    std::cout << "All Checks Pass" << std::endl;

  return !pass;
}
