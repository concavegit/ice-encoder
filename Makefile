default: build

build: rtl build_test

rtl:
	make -C rtl

build_test:
	make -C testbench

test:
	make -C testbench run

prog:
	make -C rtl prog

clean:
	make -C testbench clean
	make -C rtl clean
