`include "rs232_recv.sv"
`include "rs232_send.sv"
`include "encoder.sv"

module top
  (
   input  clk,
          RX,
          left_encoder,
          right_encoder,
   output TX,
   left_LED,
   right_LED
   );

   parameter integer BAUD_RATE = 9600;
   parameter integer CLOCK_FREQ_HZ = 12000000;
   parameter integer PERIOD = CLOCK_FREQ_HZ / BAUD_RATE;
   parameter integer WIDTH = 32;

   wire [WIDTH-1:0]  left_encoder_ticks,
                     right_encoder_ticks;
   wire              left_encoder_direction,
                     right_encoder_direction;

   rs232_recv #(.HALF_PERIOD(PERIOD / 2))
   recv_module
     (
      .clk (clk),
      .RX (RX),
      .left_encoder_direction (left_encoder_direction),
      .right_encoder_direction (right_encoder_direction)
      );

   rs232_send #(.PERIOD(PERIOD), .WIDTH(WIDTH))
   send_module
     (
      .clk (clk),
      .TX (TX),
      .left_encoder_ticks (left_encoder_ticks),
      .right_encoder_ticks (right_encoder_ticks)
      );

   encoder left_encoder_module
     (
      .signal(left_encoder),
      .direction (left_encoder_direction),
      .ticks (left_encoder_ticks)
      );

   encoder right_encoder_module
     (
      .signal(right_encoder),
      .direction (right_encoder_direction),
      .ticks (right_encoder_ticks)
      );

   assign left_LED = left_encoder_direction,
     right_LED = right_encoder_direction;
endmodule
