module encoder
  (
   input                     signal,
                             direction,
   output signed [width-1:0] ticks
   );

   parameter width=32;

   initial ticks = 0;

   always @(posedge signal)
     ticks <= ticks + (direction ? -1 : 1);
endmodule
