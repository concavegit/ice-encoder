module rs232_recv #(parameter HALF_PERIOD = 5)
   (
    input  clk,
           RX,
    output left_encoder_direction,
    right_encoder_direction
    );

   reg [1:0] buffer;
   reg       buffer_valid;

   reg [$clog2(3*HALF_PERIOD):0] cycle_cnt;
   reg [3:0]                     bit_cnt = 0;
   reg                           recv = 0;

   initial begin
      left_encoder_direction = 0;
      right_encoder_direction = 0;
   end

   always @(posedge clk) begin
      buffer_valid <= 0;

      if (!recv) begin
         if (!RX) begin
            cycle_cnt <= HALF_PERIOD;
            bit_cnt <= 0;
            recv <= 1;
         end
      end else
        if (cycle_cnt == 2 * HALF_PERIOD) begin
           cycle_cnt <= 0;
           bit_cnt <= bit_cnt + 1;

           if (bit_cnt == 9) begin
              buffer_valid <= 1;
              recv <= 0;
           end else
             if (bit_cnt < 3)
               buffer <= {RX, buffer[1]};
        end else cycle_cnt <= cycle_cnt + 1;

      if (buffer_valid) {left_encoder_direction, right_encoder_direction} <= buffer[0+:2];
   end
endmodule
