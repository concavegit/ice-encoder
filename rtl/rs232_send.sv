module rs232_send
  #(parameter PERIOD = 10,
    WIDTH = 31,
    BUFFER = 17
    ) (
       input             clk,
       output            TX,
       input [WIDTH-1:0] left_encoder_ticks,
       right_encoder_ticks
       );

   reg [$clog2(PERIOD):0] cycle_cnt = 0;
   reg [4:0]              bit_cnt = 0;
   reg [$clog2(BUFFER):0] byte_cnt = BUFFER;

   reg [WIDTH:0]          left_encoder_ticks_ff,
                          right_encoder_ticks_ff;
   always @(posedge clk) begin
      cycle_cnt <= cycle_cnt + 1;

      if (cycle_cnt == PERIOD-1) begin
         cycle_cnt <= 0;
         bit_cnt <= bit_cnt + 1;
         if (bit_cnt == 10) begin
            bit_cnt <= 0;
            byte_cnt <= byte_cnt + 1;
            left_encoder_ticks_ff <= left_encoder_ticks;
            right_encoder_ticks_ff <= right_encoder_ticks;
         end
      end
   end

   reg [7:0] data_byte;
   reg       data_bit;


   function [7:0] to_ascii;
      input [3:0] digit;
      if (digit < 10) to_ascii = "0" + digit;
      else to_ascii = "A" - 10 + digit;
   endfunction


   always_comb  begin
      data_byte = 8'bx;
      if (byte_cnt < WIDTH / 4)
        data_byte = to_ascii(left_encoder_ticks_ff[WIDTH-1-4*byte_cnt-:4]);
      else if (byte_cnt < WIDTH / 2)
        data_byte = to_ascii(right_encoder_ticks_ff[2*WIDTH-1-4*byte_cnt-:4]);
      else if (byte_cnt==WIDTH/2)
        data_byte="\n";
   end

   always @(posedge clk) begin
      data_bit <= 1'bx;
      case (bit_cnt)
        0: data_bit <= 0;
        9: data_bit <= 1;
        10: data_bit <= 1;
        default:
          data_bit <= data_byte[bit_cnt-1];
      endcase

      if (byte_cnt > WIDTH / 2) data_bit <= 1;
   end

   assign TX = data_bit;
endmodule
